package main

import (
	"fmt"
	"github.com/gorilla/mux"
	"log"
	"net/http"
)

var port = "8000"

func main() {
	fmt.Println("start.. lets check")
	log.Fatal(http.ListenAndServe(":"+port, router()))
}

func router() http.Handler {
	r := mux.NewRouter()
	r.Path("/greeting").Methods(http.MethodGet).HandlerFunc(greet)
	return r
}

func greet(w http.ResponseWriter, req *http.Request) {
	_, _ = w.Write([]byte("Hello, world!"))
}
